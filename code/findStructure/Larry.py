class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class Larry:
    def __init__(self):
        self.head = None

    def push(self, new_data):
        new_node = Node(new_data)
        new_node.next = self.head
        self.head = new_node

    def insertAfter(self, prev_node, new_data):
        if prev_node is None:
            return
        new_node = Node(new_data)
        new_node.next = prev_node.next
        prev_node.next = new_node

    def append(self, new_data):
        new_node = Node(new_data)
        if self.head is None:
            self.head = new_node
            return
        last = self.head
        while(last.next):
            last = last.next
        last.next = new_node

    def deleteNode(self, key):
        temp = self.head
        if (temp is not None):
            if (temp.data == key):
                self.head = temp.next
                temp = None
                return

        while (temp is not None):
            if (temp.data == key):
                break
            prev = temp
            temp = temp.next

        if (temp == None):
            return

        prev.next = temp.next
        temp = None

    def isEmpty(self):
        if self.head is None:
            print("No data yet! Be careful, Larry can be whatever you impress upon him to be")
    
    def printList(self):
        temp = self.head
        while(temp):
            print(temp.data)
            temp = temp.next

# The story will be told here, it'll be something simple like this demo template
if __name__ == '__main__':

    myList = Larry()
    myList.isEmpty()

    print('\nMoving on...\n')

    one = "Hello! My name is Larry"
    two = "My mother is amazing!"
    third = "My dad is all over the place"

    myList.push(one)
    myList.append(two)
    myList.append(third)
    myList.printList()

    print('\nwait...\n')
    third = "Whoops, trying to forget that part"

    myList.deleteNode("My dad is all over the place")
    myList.append(third)
    myList.printList()