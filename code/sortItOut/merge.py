import time
import arraygeneration

def mergeSort(arr):
    if len(arr) > 1:
        mid = len(arr) // 2
        
        L = arr[:mid]
        R = arr[mid:]

        mergeSort(L)
        mergeSort(R)

        i = j = k = 0

        while i < len(L) and j < len(R):
            if L[i] < R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1

        while i < len(L):
            arr[k] = L[i]
            i += 1
            k += 1

        while j < len(R):
            arr[k] = R[j]
            j += 1
            k += 1

if __name__ == '__main__':
    test_arr = arraygeneration.generateIntArray(1000, 10000)
    
    comparison_arr = test_arr.copy()
    comparison_arr.sort()
    
    print("Init array: " + str(test_arr))

    tic = time.perf_counter()
    mergeSort(test_arr)
    toc = time.perf_counter()

    print(f"Sorted array in {toc - tic:0.4f} seconds: " + str(test_arr))

    if test_arr == comparison_arr:
        print("Merge sort successfully sorted")
    else:
        print("Sorts don't match")
