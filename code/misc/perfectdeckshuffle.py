# The aim of this function will be to simulate a certain x amount of shuffles to see how close 
# a shuffled deck can get to a deck that's perfectly in order (aces, 1's, 2's, ...,  kings)
# Suits in this case will not matter
import random

target_list = [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13]

def perfectShuffle(simulations, deck):
    x = 0
    closestTotal = 0
    closestList = []
    visualList = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    totals = []
    
    while x < simulations:
        localTotal = 0
        random.shuffle(deck)

        for i in range(52):
            if deck[i] == target_list[i]:
                localTotal += 1
        
        if localTotal > closestTotal:
            #print("Hit")
            #print(localTotal, closestTotal)
            #print(deck)
            closestTotal = localTotal
            closestList = deck.copy()

        totals.append(localTotal)
        x += 1

    for j in range(52):
        if closestList[j] == target_list[j]:
            visualList[j] = target_list[j]

    print("After " + str(simulations) + " simulations, the closest match to the target list was " + str(closestTotal))
    print(closestList)
    print(visualList)

    return totals