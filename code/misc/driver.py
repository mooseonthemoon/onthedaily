import perfectdeckshuffle
import random
import statistics as s

def main():
    input_list = [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13]
    
    random.shuffle(input_list)
    
    totals = perfectdeckshuffle.perfectShuffle(1000000, input_list)

    # Stats showcase

    print("--------------------")
    print("Averages, measures, and spread:")
    print("Mean of simulation:")
    print(s.mean(totals))
    print("\nMedian:")
    print(s.median(totals))
    print("\nMode:")
    print(s.mode(totals))
    print("\nPopulation standard deviation:")
    print(s.pstdev(totals))
    print("\nPopulation variance:")
    print(s.pvariance(totals))



if __name__ == '__main__':
    main()