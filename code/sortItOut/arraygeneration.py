import random

def generateIntArray(x, maximum): # x being array size, maximum being upper bound of int that can be generated. All integers will be randomly generated from 0 to max
    final_array = []
    for count in range(x):
        insert = random.randint(0, maximum)
        final_array.append(insert)

    return final_array